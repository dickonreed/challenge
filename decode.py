from json import loads
from datetime import datetime
from pprint import pprint

def canned():
    """Load canned data for text fixtures; file created by retrieve.save"""
    with open('canned.json', 'r') as fobj:
        content = fobj.read()
    return loads(content)

def test_canned():
    assert len(canned()) == 16

def get_first_arrival_text(data):
    return data[0]['expectedArrival']

def test_get_first_arrival_text():
    assert get_first_arrival_text(canned()) == '2017-11-03T10:41:36Z'

def extract_line_arrivals(data):
    # TODO: robust against format changes?
    return [(x.get('expectedArrival'), x) for x in data]

def test_extract_line_arrivals():
    arrivals = extract_line_arrivals(canned())
    print(list(enumerate(arrivals)))
    assert len(arrivals) == 16

def decode_time_to_datetime(timestamp):
    # timezones are a pain with datetime, IIRC. So assume for now GMT or BST
    tz = timestamp[-1]
    assert tz in 'AZ'
    # TODO: consider BST: may be off by 1 hour, potential bug
    return datetime.strptime(timestamp[:-1], "%Y-%m-%dT%H:%M:%S")

def test_decode_time_to_epoch():
    dt = decode_time_to_datetime('2017-11-03T10:53:39Z')
    assert dt.year == 2017
    assert dt.second == 39
    assert dt.timestamp() == 1509706419
    
 
def get_arrivals_for_line(arrivalpairs, line):
    """
    Get sorted arrivals for specific line

    Args:
        arrivalpairs (text time, record): as returned by extract_line_arrivals
        line (str): line ID e.g. piccadilly

    Returns:
        sorted list of ( epoch_seconds, record)
    """
    return list(sorted([(decode_time_to_datetime(when).timestamp(), rec) for when, rec in arrivalpairs if line == rec['lineId']], key=lambda x: x[0]))

def test_get_first_arrivals_for_line():
    pprint(extract_line_arrivals(canned())[0])
    times = get_arrivals_for_line(extract_line_arrivals(canned()), 'piccadilly')
    assert [type(t) == type(123.0) for t in times]
    assert times[0][0] == 1509704916.0


# TODO: move announce to its own module - it feels more like business logic than decoding
def announcement(arrival, record, now):
    delay = arrival - now
    # TODO: break out minutes etc
    # TODO: cope with bad data - we assume platformName and destinationName are strings
    return '%d seconds train on %s to %s' % (delay, record['platformName'], record['destinationName'])

    
