from subprocess import check_output
from json import loads
from pprint import pprint

DEFAULT_STOPPOINT = '940GZZLUASL'
DEFAULT_LINEID = 'piccadilly'


def retrieve(stoppoint=DEFAULT_STOPPOINT):
    URL = 'https://api.tfl.gov.uk/StopPoint/'+stoppoint+'/Arrivals'
    # TODO: urlopen returns a TLSV1_ALERT_PROTOCOL_VERSION IOError, look into that
    if 0:
        from urllib import urlopen
        print(urlopen(URL, 'r').read())
    else:
        return check_output(['curl', URL])
    
def show(stoppoint=DEFAULT_STOPPOINT):
    data = loads(retrieve(stoppoint))
    pprint(data)

def save(stoppoint=DEFAULT_STOPPOINT, name='canned.json'):
    """Save out golden data to populate test suite"""
    # TODO: change to open to work in Python 3
    with file(name, 'w') as fobj:
        fobj.write(retrieve(stoppoint))
