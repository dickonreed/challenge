from argparse import ArgumentParser
from pprint import pprint
from time import time, sleep, asctime, localtime
from json import loads

from retrieve import retrieve
from update_boards import update_boards
# TODO: retreive from command line using argparse
POLL_INTERVAL = 10 # set to 60 to follow spec
LOGFILE = 'arrivals.log'

def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--stoppoint', action='store', default='940GZZLUASL')
    parser.add_argument('-l', '--lineid', action='store', default='piccadilly')
    parser.add_argument('-c', '--canned', action='store_true',
                        help='Use canned data, ignoring --stoppoint and --lineid')
    return parser.parse_args()

def log(text):
    ts = asctime(localtime(time()))
    with open(LOGFILE, 'a') as fobj:
        print(ts, text, file=fobj)
    print(ts, text)
    
def send_arrival(text):
    log('ARRIVAL:'+text)

def send_following_arrivals(textlist):
    for i, text in enumerate(textlist):
        log('FURTHER: #%d: %s' %(i, text))

# TODO: move update_boards to a file and a test case

args = parse_args()
if args.canned:
    from decode import canned
    data = canned()
    now = 1509704900
    update_boards(now, 'piccadilly', data, set(), send_arrival, send_following_arrivals)
else:
    # TODO: only announce once, and with less than a minute to go
    announced = set()
    log('starting with stoppoint %s lineid %s' % (args.stoppoint, args.lineid))
    while True:
        now = time()
        data = loads(retrieve(args.stoppoint))
        announced = update_boards(now, args.lineid, data, announced, send_arrival, send_following_arrivals)
        sleep(POLL_INTERVAL)


    
