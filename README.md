Interface to TFL and make arrival announcements

Example URL to parse:

https://api.tfl.gov.uk/StopPoint/940GZZLUASL/Arrivals

(Sample output from that, used as golden data for testing is in canned.json)

Requirements:

 - Python 3 (developed on 3.6.3 on OSX 10.10.5)
 - py.test (developed on 3.2.3 installed via pip)

TODO:
 - cope with timezones, summer time
 - add python package structure
 - robustness against input format changes, missing data
 - pull out update_boards and add test code for it
 - more detailed arrivals information?
 - what happens around midnight, timezone transitions?
 
Test cycle:
  py.test decode.py && python3 arrivalsboard.py
  