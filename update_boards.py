from decode import get_arrivals_for_line, extract_line_arrivals, announcement, canned

def update_boards(now, lineid, content, announced, invoke_arrival, invoke_following_arrivals,
                  announce_threshold=60):
    """Given some data invoke a couple of callbacks as approrpriate

    Args:
        now (number): current time
        lineid (str): lineid format matching TFL format
        content (list of dictionaries): TFL data output
        announced (set of string): the set of stations we've already invoked
        invoke_arrival (function string->None): callback when a train is due to arrive
        invoke_following_arrivals (function list(string)->None): callback with new announcements
        announce_threshold(number): time before arrival to announce train

    Returns:
        set of string: updated copy of announced with any new announcement added
    """
    nowannounced = set(announced)
    arrivals = get_arrivals_for_line(extract_line_arrivals(content), lineid)
    nextepoch, nextrec = arrivals[0]

    delay = nextepoch-now
    # TODO: do we announce trains in the past
    if delay < announce_threshold and nextrec['id'] not in announced:
        invoke_arrival(announcement(nextepoch, nextrec, now))
        nowannounced.add(nextrec['id'])

    # TODO: should we return the next train always, or only when unannounced?
    # TODO: filter out past trains
    invoke_following_arrivals([announcement(altepoch, altrec, now) for altepoch, altrec in arrivals if altepoch > now])
    return nowannounced


def test_update_boards():
    arr = []
    following = []
    update_boards(0, 'piccadilly', canned(), set(), lambda x: arr.append(x), lambda x: following.append(x))
    assert len(arr) == 0
    # TODO: test a nearby train, is it announced?
    # TODO: test bad data
    # TODO: verify format of announcement
    # TODO: check lines
    # TODO: try other stations which may be more complex
    
